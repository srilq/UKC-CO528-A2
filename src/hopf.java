import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

public class hopf {

    // increase print level to print more verbose runtime information
    private static final int CONFIG_PRINT_LEVEL = 0;

    private static int errorcode = 0;

    /**
     * Program entry point.
     * @param args Program arguments given as array of Strings
     */
    public static void main(String[] args)
    {
        if (args.length != 2)
        {
            println("Error: incorrect number of arguments.\nUsage: hopf [STP] [INC]\n", true);
            errorcode++;
            System.exit(errorcode);
            return;
        }

        String stpFilename = args[0];
        String incFilename = args[1];

        String stpFile;
        String incFile;
        try {
            stpFile = readFile(stpFilename);
            incFile = readFile(incFilename);
        } catch (IOException e) {
            println("Error reading file: " + e.getMessage(), true);
            errorcode++;
            System.exit(errorcode);
            return;
        }
        if (CONFIG_PRINT_LEVEL >= 4)
        {
            println("STORED PATTERNS FILE CONTENTS:");
            print(stpFile);
            println("INCOMPLETE PATTERNS FILE CONTENTS:");
            print(incFile);
        }

        int[][] stp;
        int[][] inc;
        try {
            stp = parseFile(stpFile);
            inc = parseFile(incFile);
        } catch (ParseException e) {
            println("Error parsing file on line " + e.getErrorOffset() + ": " + e.getMessage());
            errorcode++;
            System.exit(errorcode);
            return;
        }
        if (CONFIG_PRINT_LEVEL >= 2)
        {
            printIntMatrix(stp, "STORED PATTERNS:");
            printIntMatrix(inc, "INCOMPLETE PATTERNS:");
        }

        float[][] weights = calcWeights(stp);
        if (CONFIG_PRINT_LEVEL >= 3) printFloatMatrix(weights, "WEIGHTS:");

        int[][] calcInc = calcPatterns(inc, weights);
        printIntMatrix(calcInc, CONFIG_PRINT_LEVEL >= 2 ? "CALCULATED PATTERNS:" : "", true);

        System.exit(0);
    }

    ///
    /// OPERATIONS
    ///

    /**
     * Calculate weights for the given complete pattern.
     * @param pattern
     * @return
     */
    private static float[][] calcWeights(int[][] pattern)
    {
        int tar_rows = pattern.length;
        int tar_cols = pattern[0].length;

        float[][] weights = new float[tar_cols][tar_cols];

        for (int tar_r = 0; tar_r < tar_rows; tar_r++)
        {
            for (int tar_c = 0; tar_c < tar_cols; tar_c++)
            {
                for (int tar_c2 = 0; tar_c2 < tar_cols; tar_c2++)
                {
                    if (tar_c == tar_c2) continue;
                    weights[tar_c][tar_c2] += pattern[tar_r][tar_c] * pattern[tar_r][tar_c2];
                }
            }
        }

        for (int tar_c = 0; tar_c < tar_cols; tar_c++)
        {
            for (int tar_c2 = 0; tar_c2 < tar_cols; tar_c2++)
            {
                weights[tar_c][tar_c2] /= tar_rows;
            }
        }

        return weights;
    }

    /**
     * Calculate a learned pattern with the given pattern and weights.
     * Has deja vu with a short-term memory buffer which stores previous patterns.
     * @param patterns
     * @param weights
     * @return
     */
    private static int[][] calcPatterns(int[][] patterns, float[][] weights)
    {
        if (patterns.length <= 0) return patterns;

        int pat_rows = patterns.length;
        int pat_cols = patterns[0].length;

        for (int pat_r = 0; pat_r < pat_rows; pat_r++)
        {
            int[] prev = patterns[pat_r];
            rowCalc : while (true)
            {
                for (int pat_c = 0; pat_c < pat_cols; pat_c++)
                {
                    float sum = 0;
                    for (int pat_c2 = 0; pat_c2 < pat_cols; pat_c2++)
                    {
                        if (pat_c == pat_c2) continue;
                        sum += weights[pat_c][pat_c2] * patterns[pat_r][pat_c2];
                    }
                    patterns[pat_r][pat_c] = sum >= 0 ? 1 : -1;
                }
                if (Arrays.equals(patterns[pat_r], prev)) break rowCalc;
                prev = patterns[pat_r];
            }
        }

        return patterns;
    }

    ///
    /// UTIL
    ///

    /**
     * Parse the given file contents and output a pattern matrix.
     * @param file
     * @return
     * @throws ParseException
     */
    private static int[][] parseFile(String file) throws ParseException
    {
        String[] fileRows = file.split("\\n");
        int rows = fileRows.length;
        if (rows < 1 || file.length() <= 0) throw new ParseException("File is empty.", 0);

        int cols = fileRows[0].split("\\s").length;
        for (int r = 0; r < fileRows.length; r++)
        {
            String[] fileCols = fileRows[r].split("\\s");
            if (fileCols.length != cols) throw new ParseException("Rows do not all have equal number of columns.", r + 1);
        }

        int[][] matrix = new int[rows][cols];
        for (int r = 0; r < rows; r++)
        {
            String[] fileCols = fileRows[r].split("\\s");
            for (int c = 0; c < cols; c++)
            {
                matrix[r][c] = Integer.parseInt(fileCols[c]);
            }
        }
        return matrix;
    }

    /**
     * Read the given filename to a string.
     * @param filename
     * @return
     * @throws IOException
     */
    private static String readFile(String filename) throws IOException
    {
        String file = "";
        FileReader fReader;
        fReader = new FileReader(filename);
        BufferedReader reader = new BufferedReader(fReader);
        while (true)
        {
            String line = reader.readLine();
            if (line == null) break;
            file += line + "\n";
        }
        return file;
    }

    /**
     * Print the given float matrix with a prefix printed beforehand.
     * @param matrix
     * @param prefix
     * @param force
     */
    private static void printFloatMatrix(float[][] matrix, String prefix, boolean force)
    {
        if (prefix.length() > 0) println(prefix, force);
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[i].length; j++)
            {
                float val = matrix[i][j];
                String out = (val >= 0 ? ("+" + val) : val).toString().substring(0,4);
                if (j < matrix[i].length - 1) out += " ";
                print(out, force);
            }
            println(force);
        }
    }
    /**
     * Print the given float matrix with a prefix printed beforehand.
     * @param matrix
     * @param prefix
     */
    private static void printFloatMatrix(float[][] matrix, String prefix)
    {
        printFloatMatrix(matrix, prefix, false);
    }
    /**
     * Print the given float matrix.
     * @param matrix
     * @param force
     */
    private static void printFloatMatrix(float[][] matrix, boolean force)
    {
        printFloatMatrix(matrix, "", force);
    }
    /**
     * Print the given float matrix.
     * @param matrix
     */
    private static void printFloatMatrix(float[][] matrix)
    {
        printFloatMatrix(matrix, false);
    }

    /**
     * Print the given int matrix with a prefix printed beforehand.
     * @param matrix
     * @param prefix
     * @param force
     */
    private static void printIntMatrix(int[][] matrix, String prefix, boolean force)
    {
        if (prefix.length() > 0) println(prefix, force);
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[i].length; j++)
            {
                int val = matrix[i][j];
                String out = val + "";
                if (j < matrix[i].length - 1) out += " ";
                print(out, force);
            }
            println(force);
        }
    }
    /**
     * Print the given int matrix with a prefix printed beforehand.
     * @param matrix
     * @param prefix
     */
    private static void printIntMatrix(int[][] matrix, String prefix)
    {
        printIntMatrix(matrix, prefix, false);
    }
    /**
     * Print the given int matrix.
     * @param matrix
     * @param force
     */
    private static void printIntMatrix(int[][] matrix, boolean force)
    {
        printIntMatrix(matrix, "", force);
    }
    /**
     * Print the given int matrix.
     * @param matrix
     */
    private static void printIntMatrix(int[][] matrix)
    {
        printIntMatrix(matrix, false);
    }

    /**
     * Print the given string if the print level is greater than 0.
     * @param string
     * @param force
     */
    private static void print(String string, boolean force)
    {
        if (force || CONFIG_PRINT_LEVEL > 0) System.out.print(string);
    }
    /**
     * Print the given string if the print level is greater than 0.
     * @param string
     */
    private static void print(String string)
    {
        print(string, false);
    }
    /**
     * Print the given string if the print level is greater than 0.
     * Adds a neewline after the string.
     * @param string
     * @param force
     */
    private static void println(String string, boolean force)
    {
        print(string + "\n", force);
    }
    /**
     * Print the given string if the print level is greater than 0.
     * Adds a neewline after the string.
     * @param string
     */
    private static void println(String string)
    {
        println(string, false);
    }
    /**
     * Print a newline if the print level is greater than 0.
     * @param force
     */
    private static void println(boolean force)
    {
        print("\n", force);
    }
    /**
     * Print a newline if the print level is greater than 0.
     */
    private static void println()
    {
        println(false);
    }
}
